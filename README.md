First, run "npm install" on the command prompt. Update your config.env file by changing MONGO_URI from MongoDB atlas. Finally, run "nodemon server" on the command prompt
![Screenshot__25_](/uploads/6d412a4b9e173ea046a031af07cbec0f/Screenshot__25_.png)
![Screenshot__33_](/uploads/b6ed925d02c4b58df0832f706843090d/Screenshot__33_.png)
